#!/bin/bash

if [ $# -lt 2 ]
then
    echo "Not enough arguments supplied"
    exit
fi

input_dir=$1
this_script=`basename $0`
files_except_script=`ls $input_dir --ignore=$this_script`
files_with_match=$(ls --ignore=$this_script | grep -l "$2" $input_dir"/"$files_except_script)

if [[ $? -eq 1 ]]
then
    echo "No files with matching string found!"
    exit 
fi

echo "Substring have been found in files:" $files_with_match

for file in $files_with_match
do
    echo "Removing file" $file
    rm $file
done


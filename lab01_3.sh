#!/bin/bash

if [ $# -eq 0 ]
then
    echo "No arguments supplied"
    exit
fi

text_files=`find . -name \*.txt`
output_file=$1

for filename in $text_files
do
    echo `basename $filename`
done > $output_file

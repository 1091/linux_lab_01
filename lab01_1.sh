#!/bin/bash

if [ $# -lt 2 ]
then
    echo "Not enough arguments supplied"
    exit
fi

input_dir=$1
file_reverse=$2
echo `ls $input_dir | sort -r` > $file_reverse

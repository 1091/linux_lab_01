#!/bin/bash

if [ $# -eq 0 ]
then
    echo "No arguments supplied"
    exit
fi

this_script=`basename $0`
files_except_script=`ls --ignore=$this_script`
files_with_match=$(ls --ignore=$this_script | grep -r -l "$1" $files_except_script)

if [[ $? -eq 1 ]]
then
    echo "No files with matching string found!"
    exit 
fi

echo "Substring have been found in files:" $files_with_match

counter=0
for file in $files_with_match
do
    let counter++
done

echo $counter "files with match have been found in directory"

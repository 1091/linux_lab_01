#!/bin/bash

if [ $# -eq 0 ]
then
    echo "No arguments supplied"
    exit
fi

hostname=$1
username=$2
password=$3

remote_files=`ftp -i -n $hostname << EOF
user 53web2 ibanezgrx20jb
nlist
EOF`

for remote_file in $remote_files
do
    if [ $remote_file != "." ] && [ $remote_file != ".." ];
    then
       let counter++
       echo "["$counter"]" $remote_file
       remote_files_array[counter]=$remote_file
    fi
done

not_answered=0
while [ $not_answered -ne 1 ]
do
    read -p "Which file to download? Choose number: " file_number

    if [ $file_number -gt $counter ] || [ $file_number -lt 1 ];
    then
        echo "Wrong number - out of range."
    else
        not_answered=1
    fi
done

echo "Downloading file:" ${remote_files_array[$file_number]} 
filename=${remote_files_array[$file_number]}
ftp -i -n $hostname << EOF
user 53web2 ibanezgrx20jb
get $filename
EOF
echo "Done downloading."

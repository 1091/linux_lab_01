Installation
Download and unzip/untar the pyjs.tar.gz file

Change into the pyjs root directory
cd <pyjsroot>

Optional step to create your own virtual python sandbox for development purposes virtualenv mypython

This will create a mypython directory and install sandboxed virtual python installation at
<pyjsroot>/mypython
Install Pyjs-Tools, Pyjs-Pyjamas and all dependencies
<pyjsroot>/mypython/bin/pip install <pyjsroot>

OR the following if you want to develop and modify Pyjs-Compiler or Pyjs-Widgets themselves

<pyjsroot>/mypython/bin/pip install -e <pyjsroot>
You are now ready to use Pyjs. Scripts for compiling python code to Javascript as well as compiling applications will be installed into the mypython sandbox

<pyjsroot>/mypython/bin/pyjscompile <pyjsroot>/mypython/bin/pyjsbuild <pyjsroot>/mypython/bin/pyjampiler
